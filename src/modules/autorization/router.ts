import Module from "./Module.vue";
import Login from "./login/Login.vue";
import Registration from "./registration/Registration.vue";

export const routes = [
  {
    path: "/auth",
    name: "auth",
    component: Module,
    children: [
      {
        path: "login",
        name: "login",
        component: Login,
      },
      {
        path: "registration",
        name: "registration",
        component: Registration,
      },
    ],
  },
];
