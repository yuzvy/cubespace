import Module from "./Module.vue";
import List from "./views/Index.vue";
import Ticket from "./views/Ticket.vue";

export const routes = [
  {
    path: "/tickets",
    name: "ticket",
    component: Module,
    children: [
      {
        path: "",
        name: "tickets-main",
        component: List,
      },
      {
        path: ":id",
        name: "ticket-detail",
        component: Ticket,
        props: true,
      },
    ],
  },
];
