import { createRouter, createWebHistory } from "vue-router";

import mSplash from "../modules/splash";
import mAuth from "../modules/autorization";
import mDashboard from "../modules/dashboard";
import mTickets from "../modules/tickets";
import mOffice from "../modules/office";

const routes = [
  ...mSplash.routes,
  ...mAuth.routes,
  ...mDashboard.routes,
  ...mTickets.routes,
  ...mOffice.routes,
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
