import { CapacitorConfig } from "@capacitor/cli";

const config: CapacitorConfig = {
  appId: "cubeback.com",
  appName: "CubeDashboard",
  webDir: "dist",
  bundledWebRuntime: false,
};

export default config;
